The Debian packaging of cvm is maintained using dgit.  For the sake
of an efficient workflow, Debian modifications to the upstream
source are squashed into a single diff, rather than a series of
quilt patches.  To obtain a patch queue for package version
0.97-0.1:

    # apt-get install dgit
    % dgit clone cvm
    % cd foo
    % git log --oneline 0.97-0.1..debian/0.97 -- . ':!debian'
---
 Makefile |  2 +-
 tests.sh | 78 ++++++++++++++++++++++++++++++++++++----------------------------
 2 files changed, 45 insertions(+), 35 deletions(-)

diff --git a/Makefile b/Makefile
index 9abbee1..3d9b7a8 100644
diff --git a/tests.sh b/tests.sh
index 689b6f1..9b1add0 100644
--- cvm-0.97.orig/Makefile
+++ cvm-0.97/Makefile
@@ -368,7 +368,7 @@ sql-query-test: sql-query-test.o load sq
 sql-query-test.o: compile sql-query-test.c credentials.h sql.h
 	./compile sql-query-test.c
 
-sql-query.lo sql-query.o: ltcompile sql-query.c module.h credentials.h errors.h facts.h sql.h
+sql-query.lo sql-query.o &: ltcompile sql-query.c module.h credentials.h errors.h facts.h sql.h
 	./ltcompile sql-query.c
 
 sqlite: cvm-sqlite
--- cvm-0.97.orig/tests.sh
+++ cvm-0.97/tests.sh
@@ -161,34 +161,38 @@ $src/cvm-testclient $src/cvm-sqlite slus
 
 rm $CVM_SQLITE_DB
 }
-vecho "Running test tests/sqlite "
-run_compare_test tests/sqlite  <<END_OF_TEST_RESULTS
-user name:        sluser
-user ID:          123
-group ID:         456
-real name:        realname
-directory:        /home/sluser
-shell:            /bin/false
-group name:       (null)
-system user name: (null)
-system directory: (null)
-domain:           
-mailbox path:     (null)
 
-cvm-testclient: Authentication failed, error #100 (Credentials rejected)
+# In the Debian packaging, sqlite cvm tools are excluded from the build.
+# Excluding this test as well to make the test suite work.
 
-user name:        sluser
-user ID:          234
-group ID:         567
-real name:        sluser2
-directory:        /home/sluser2
-shell:            /bin/true
-group name:       (null)
-system user name: (null)
-system directory: (null)
-domain:           domain
-mailbox path:     (null)
-END_OF_TEST_RESULTS
+# vecho "Running test tests/sqlite "
+# run_compare_test tests/sqlite  <<END_OF_TEST_RESULTS
+# user name:        sluser
+# user ID:          123
+# group ID:         456
+# real name:        realname
+# directory:        /home/sluser
+# shell:            /bin/false
+# group name:       (null)
+# system user name: (null)
+# system directory: (null)
+# domain:           
+# mailbox path:     (null)
+#
+# cvm-testclient: Authentication failed, error #100 (Credentials rejected)
+#
+# user name:        sluser
+# user ID:          234
+# group ID:         567
+# real name:        sluser2
+# directory:        /home/sluser2
+# shell:            /bin/true
+# group name:       (null)
+# system user name: (null)
+# system directory: (null)
+# domain:           domain
+# mailbox path:     (null)
+# END_OF_TEST_RESULTS
 
 
 ##### Test tests/qmail-lookup-assume #####
@@ -297,12 +301,15 @@ $src/cvm-testclient $src/cvm-vmailmgr vi
 
 chmod 444 $home/passwd.cdb
 }
-vecho "Running test tests/vmlookup-notable "
-run_compare_test tests/vmlookup-notable  <<END_OF_TEST_RESULTS
+
+if [ ${_UID} -ne 0 ]; then
+    vecho "Running test tests/vmlookup-notable "
+    run_compare_test tests/vmlookup-notable  <<END_OF_TEST_RESULTS
 cvm-testclient: Authentication failed, error #100 (Credentials rejected)
 out of scope:     1
 cvm-testclient: Authentication failed, error #4 (Input/Output error)
 END_OF_TEST_RESULTS
+fi
 
 
 ##### Test tests/pwfile-crypt #####
@@ -674,9 +681,9 @@ system directory: @TMPDIR@/home
 domain:           test.tld
 mailbox path:     @TMPDIR@/home/virt
 
-0000000: 020a 0108 0100 2430 2470 6173 7300 2e2f  ......\$0\$pass../
-0000010: 7669 7274 0000 002d 002d 002d 002d 0031  virt...-.-.-.-.1
-0000020: 3031 3637 3331 3335 3800 2d00            016731358.-.
+00000000: 020a 0108 0100 2430 2470 6173 7300 2e2f  ......\$0\$pass../
+00000010: 7669 7274 0000 002d 002d 002d 002d 0031  virt...-.-.-.-.1
+00000020: 3031 3637 3331 3335 3800 2d00            016731358.-.
 END_OF_TEST_RESULTS
 
 
@@ -1054,8 +1061,10 @@ chmod 755 $home
 unset CVM_LOOKUP_SECRET
 unset CVM_QMAIL_CHECK_PERMS
 }
-vecho "Running test tests/qmail-lookup-perms "
-run_compare_test tests/qmail-lookup-perms  <<END_OF_TEST_RESULTS
+
+if [ ${_UID} -ne 0 ]; then
+    vecho "Running test tests/qmail-lookup-perms "
+    run_compare_test tests/qmail-lookup-perms  <<END_OF_TEST_RESULTS
 user name:        user
 user ID:          @UID@
 group ID:         @GID@
@@ -1073,6 +1082,7 @@ cvm-testclient: Authentication failed, e
 cvm-testclient: Authentication failed, error #100 (Credentials rejected)
 out of scope:     0
 END_OF_TEST_RESULTS
+fi
 
 
 ##### Test tests/v1lookup #####
