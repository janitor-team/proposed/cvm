#!/bin/sh
src=`pwd`
tmp=$src/tests-tmp
rm -rf $tmp
mkdir -p $tmp
PATH="$src:/bin:/usr/bin:/usr/local/bin"
tests_failed=0
tests_count=0
_UID=`id -u`
_GID=`id -g`

usage() {
  echo "usage: sh $0 [-v]"
}

vecho() { :; }
while getopts v flag
do
  case $flag in
    v)      vecho() { echo "$*"; } ;;
	*)      usage; exit 1 ;;
  esac
done
printf='env printf'

home=$tmp/home
mkdir $home

CVM_PWFILE_PATH=$tmp/pwfile
export CVM_PWFILE_PATH
make_pwfile()
{
  (
    echo 'pwfuser:testpass:123:456:Gecos,xyz:/home/ftp:/bin/false'
    echo 'cryptuser:tpzv1IkcX9.fE:234:567:Crypt:/home/crypt:/bin/true'
  ) >$CVM_PWFILE_PATH
}

CVM_SQLITE_DB=$tmp/db.sqlite
CVM_SQLITE_QUERY='SELECT password, username, userid, groupid, directory, realname, shell, groupname, domain, sys_username, sys_directory, mailbox_path FROM accounts WHERE username=$account AND domain=$domain'
CVM_SQLITE_PWCMP=plain
export CVM_SQLITE_DB CVM_SQLITE_QUERY CVM_SQLITE_PWCMP
make_sqlite()
{
  sqlite3 $CVM_SQLITE_DB <<EOF
CREATE TABLE accounts (
  username      TEXT NOT NULL,
  domain        TEXT NOT NULL,
  password      TEXT NOT NULL,
  userid        TEXT NOT NULL,
  groupid       TEXT NOT NULL,
  realname      TEXT,
  directory     TEXT NOT NULL,
  shell         TEXT,
  groupname     TEXT,
  sys_username  TEXT,
  sys_directory TEXT,
  mailbox_path  TEXT,
  UNIQUE (username, domain)
);
INSERT INTO accounts VALUES ('sluser','','testpass',123,456,'realname','/home/sluser','/bin/false',NULL,NULL,NULL,NULL);
INSERT INTO accounts VALUES ('sluser','domain','pass2',234,567,'sluser2','/home/sluser2','/bin/true',NULL,NULL,NULL,NULL);
EOF
}

for base in pwfile qmail vmailmgr; do

cat <<EOF >$tmp/cvm-$base-lookup
#!/bin/sh
CVM_LOOKUP_SECRET=secret export CVM_LOOKUP_SECRET
exec $src/cvm-$base
EOF

cat <<EOF >$tmp/cvm-$base-nosecret
#!/bin/bash
unset CVM_LOOKUP_SECRET
exec $src/cvm-$base
EOF

done

chmod +x $tmp/cvm-*

QMAIL_ROOT=$tmp
export QMAIL_ROOT

mkdir $tmp/users
(
  uid=`id -u`
  gid=`id -g`
  hlen=`echo $home $uid $gid | wc -c`
  tlen=`echo $tmp $uid $gid | wc -c`
  $printf "+6,%d:!user\0->user\000$uid\000$gid\000$home\0\0\n" $(($hlen+6))
  $printf "+6,%d:!user-->user\000$uid\000$gid\000$home\0-\0\n" $(($hlen+7))
  $printf "+7,%d:!alias\0->alias\000$uid\000$gid\000$tmp/alias\0\0\n" $(($tlen+13))
  $printf "+7,%d:!alias-->alias\000$uid\000$gid\000$tmp/alias\0-\0\n" $(($tlen+14))
  $printf "+1,%d:!->alias\000$uid\000$gid\000$tmp/alias\0-\0\n" $(($tlen+14))
  echo
) | cdbmake $tmp/users/cdb $tmp/users/tmp

mkdir $tmp/control
echo local.dom >$tmp/control/locals
echo test.tld:user >$tmp/control/virtualdomains
echo noathost:user >>$tmp/control/virtualdomains
echo noathost >$tmp/control/envnoathost
mkdir $tmp/alias
echo \# >$home/.qmail-wild-default
echo \# >$home/.qmail-addr
echo \# >$home/.qmail-dot:addr
echo \# >$tmp/alias/.qmail-alias
echo \# >$tmp/alias/.qmail-awild-default
echo \# >$tmp/alias/.qmail-adot:addr

(
  $printf '+4,69:virt->\x2\x8\x1\0$1$aSoIrl/J$TmAwoxKzrPJ0IaW5UvX4A0\0./virt\0\0\0-\0-\0-\0-\0001016731358\0-\0\n'
  echo
) | cdbmake $home/passwd.cdb $home/passwd.tmp

sasltest() {
  make_pwfile
  env \
  CVM_SASL_PLAIN=$src/cvm-pwfile \
  PROTO=TCP \
  TCPLOCALHOST=localhost \
  $src/sasl-auth-test "$@"
  rm -f $CVM_PWFILE_PATH
}


run_compare_test() {
  local name=$1
  shift
  sed -e "s:@SOURCE@:$src:g"   	-e "s:@TMPDIR@:$tmp:g"   	-e "s:@UID@:$_UID:" 	-e "s:@GID@:$_GID:" 	>$tmp/expected
  ( runtest "$@" 2>&1 ) 2>&1 >$tmp/actual-raw
  cat -v $tmp/actual-raw >$tmp/actual
  if ! cmp $tmp/expected $tmp/actual >/dev/null 2>&1
  then
    echo "Test $name $* failed:"
	( cd $tmp; diff -U 9999 expected actual | tail -n +3; echo; )
	tests_failed=$(($tests_failed+1))
  fi
  rm -f $tmp/expected $tmp/actual
  tests_count=$(($tests_count+1))
}

##### Test tests/sqlite #####

runtest() {
make_sqlite

# Can it look up the right user
$src/cvm-testclient $src/cvm-sqlite sluser '' testpass

echo
# Does it check the password
$src/cvm-testclient $src/cvm-sqlite sluser '' testpasx

echo
# Does it look up the domain
$src/cvm-testclient $src/cvm-sqlite sluser domain pass2

rm $CVM_SQLITE_DB
}
vecho "Running test tests/sqlite "
run_compare_test tests/sqlite  <<END_OF_TEST_RESULTS
user name:        sluser
user ID:          123
group ID:         456
real name:        realname
directory:        /home/sluser
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           
mailbox path:     (null)

cvm-testclient: Authentication failed, error #100 (Credentials rejected)

user name:        sluser
user ID:          234
group ID:         567
real name:        sluser2
directory:        /home/sluser2
shell:            /bin/true
group name:       (null)
system user name: (null)
system directory: (null)
domain:           domain
mailbox path:     (null)
END_OF_TEST_RESULTS


##### Test tests/qmail-lookup-assume #####

runtest() {
env CVM_LOOKUP_SECRET=secret \
$src/cvm-testclient $tmp/cvm-qmail-lookup user nothingness

echo

env CVM_LOOKUP_SECRET=secret CVM_QMAIL_ASSUME_LOCAL=1 \
$src/cvm-testclient $tmp/cvm-qmail-lookup user nothingness
}
vecho "Running test tests/qmail-lookup-assume "
run_compare_test tests/qmail-lookup-assume  <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1

user name:        user
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           nothingness
mailbox path:     @TMPDIR@/home
END_OF_TEST_RESULTS


##### Test tests/vmlookup-normal #####

runtest() {
CVM_LOOKUP_SECRET=secret \
$src/cvm-testclient $tmp/cvm-vmailmgr-lookup virt test.tld
}
vecho "Running test tests/vmlookup-normal "
run_compare_test tests/vmlookup-normal  <<END_OF_TEST_RESULTS
user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt
END_OF_TEST_RESULTS


##### Test tests/sql-substitution #####

runtest() {
sqt() { env - var1=1 var_2=2 $src/sql-query-test "$@"; }
sqt 'one two' act dom
sqt '$account $domain one two' act dom
sqt 'one $account two' act dom
sqt 'one two $account' act dom
sqt '$var1 one two' act dom
sqt 'one $var1 two' act dom
sqt 'one two $var1' act dom
sqt '$var_2 one two' act dom
sqt 'one $var_2 two' act dom
sqt 'one two $var_2' act dom
sqt '$var1one two' act dom
sqt 'one $var1two' act dom
sqt '${var1} one two' act dom
sqt 'one ${var1} two' act dom
sqt 'one two ${var1}' act dom
}
vecho "Running test tests/sql-substitution "
run_compare_test tests/sql-substitution  <<END_OF_TEST_RESULTS
one two
'act' 'dom' one two
one 'act' two
one two 'act'
'1' one two
one '1' two
one two '1'
'2' one two
one '2' two
one two '2'
 two
one 
'1' one two
one '1' two
one two '1'
END_OF_TEST_RESULTS


##### Test tests/vmlookup-notable #####

runtest() {
mv $home/passwd.cdb $home/passwd.cdb.donotuse

$src/cvm-testclient $src/cvm-vmailmgr virt test.tld 'xpass'

mv $home/passwd.cdb.donotuse $home/passwd.cdb
chmod 000 $home/passwd.cdb

$src/cvm-testclient $src/cvm-vmailmgr virt test.tld 'xpass'

chmod 444 $home/passwd.cdb
}
vecho "Running test tests/vmlookup-notable "
run_compare_test tests/vmlookup-notable  <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1
cvm-testclient: Authentication failed, error #4 (Input/Output error)
END_OF_TEST_RESULTS


##### Test tests/pwfile-crypt #####

runtest() {
make_pwfile
CVM_PWFILE_PWCMP=crypt export CVM_PWFILE_PWCMP
$src/cvm-testclient $src/cvm-pwfile cryptuser '' testpass
}
vecho "Running test tests/pwfile-crypt "
run_compare_test tests/pwfile-crypt  <<END_OF_TEST_RESULTS
user name:        cryptuser
user ID:          234
group ID:         567
real name:        Crypt
directory:        /home/crypt
shell:            /bin/true
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
END_OF_TEST_RESULTS


##### Test tests/vmlookup-upper-virt #####

runtest() {
CVM_LOOKUP_SECRET=secret \
$src/cvm-testclient $tmp/cvm-vmailmgr-lookup Virt test.tld
}
vecho "Running test tests/vmlookup-upper-virt "
run_compare_test tests/vmlookup-upper-virt  <<END_OF_TEST_RESULTS
user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt
END_OF_TEST_RESULTS


##### Test tests/qmail-lookup-novirtualdomains #####

runtest() {
mv $QMAIL_ROOT/control/virtualdomains $QMAIL_ROOT/control/vds

env CVM_LOOKUP_SECRET=secret \
$src/cvm-testclient $tmp/cvm-qmail-lookup user test.tld

mv $QMAIL_ROOT/control/vds $QMAIL_ROOT/control/virtualdomains
}
vecho "Running test tests/qmail-lookup-novirtualdomains "
run_compare_test tests/qmail-lookup-novirtualdomains  <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1
END_OF_TEST_RESULTS


##### Test tests/qmail-lookup #####

runtest() {
local addr="$1"
local domain="$2"

env CVM_LOOKUP_SECRET=secret \
$src/cvm-testclient $tmp/cvm-qmail-lookup $addr $domain
}
vecho "Running test tests/qmail-lookup 'user' 'local.dom'"
run_compare_test tests/qmail-lookup 'user' 'local.dom' <<END_OF_TEST_RESULTS
user name:        user
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           local.dom
mailbox path:     @TMPDIR@/home
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'user-addr' 'local.dom'"
run_compare_test tests/qmail-lookup 'user-addr' 'local.dom' <<END_OF_TEST_RESULTS
user name:        user
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           local.dom
mailbox path:     @TMPDIR@/home
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'user-addr-x' 'local.dom'"
run_compare_test tests/qmail-lookup 'user-addr-x' 'local.dom' <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     0
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'user-wild' 'local.dom'"
run_compare_test tests/qmail-lookup 'user-wild' 'local.dom' <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     0
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'user-wild-x' 'local.dom'"
run_compare_test tests/qmail-lookup 'user-wild-x' 'local.dom' <<END_OF_TEST_RESULTS
user name:        user
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           local.dom
mailbox path:     @TMPDIR@/home
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'user-wild-x-y-z' 'local.dom'"
run_compare_test tests/qmail-lookup 'user-wild-x-y-z' 'local.dom' <<END_OF_TEST_RESULTS
user name:        user
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           local.dom
mailbox path:     @TMPDIR@/home
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'alias' 'local.dom'"
run_compare_test tests/qmail-lookup 'alias' 'local.dom' <<END_OF_TEST_RESULTS
user name:        alias
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/alias
shell:            (null)
group name:       (null)
system user name: alias
system directory: @TMPDIR@/alias
domain:           local.dom
mailbox path:     @TMPDIR@/alias
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'alias-x' 'local.dom'"
run_compare_test tests/qmail-lookup 'alias-x' 'local.dom' <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     0
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'awild' 'local.dom'"
run_compare_test tests/qmail-lookup 'awild' 'local.dom' <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     0
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'awild-x' 'local.dom'"
run_compare_test tests/qmail-lookup 'awild-x' 'local.dom' <<END_OF_TEST_RESULTS
user name:        alias
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/alias
shell:            (null)
group name:       (null)
system user name: alias
system directory: @TMPDIR@/alias
domain:           local.dom
mailbox path:     @TMPDIR@/alias
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'USER' 'local.dom'"
run_compare_test tests/qmail-lookup 'USER' 'local.dom' <<END_OF_TEST_RESULTS
user name:        user
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           local.dom
mailbox path:     @TMPDIR@/home
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'user-ADDR' 'local.dom'"
run_compare_test tests/qmail-lookup 'user-ADDR' 'local.dom' <<END_OF_TEST_RESULTS
user name:        user
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           local.dom
mailbox path:     @TMPDIR@/home
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'USER-ADDR' 'local.dom'"
run_compare_test tests/qmail-lookup 'USER-ADDR' 'local.dom' <<END_OF_TEST_RESULTS
user name:        user
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           local.dom
mailbox path:     @TMPDIR@/home
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'user-dot.addr' 'local.dom'"
run_compare_test tests/qmail-lookup 'user-dot.addr' 'local.dom' <<END_OF_TEST_RESULTS
user name:        user
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           local.dom
mailbox path:     @TMPDIR@/home
END_OF_TEST_RESULTS

vecho "Running test tests/qmail-lookup 'adot.addr' 'local.dom'"
run_compare_test tests/qmail-lookup 'adot.addr' 'local.dom' <<END_OF_TEST_RESULTS
user name:        alias
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/alias
shell:            (null)
group name:       (null)
system user name: alias
system directory: @TMPDIR@/alias
domain:           local.dom
mailbox path:     @TMPDIR@/alias
END_OF_TEST_RESULTS


##### Test tests/chain-client #####

runtest() {
$src/cvm-testclient $src/cvm-pwfile,$src/cvm-vmailmgr pwfuser test.tld testpass
echo
$src/cvm-testclient $src/cvm-pwfile,$src/cvm-vmailmgr virt test.tld pass
echo
$src/cvm-testclient $src/cvm-pwfile,$src/cvm-vmailmgr notexist test.tld pass
echo
$src/cvm-testclient $src/cvm-pwfile,$src/cvm-vmailmgr notexist other.dom pass
}
vecho "Running test tests/chain-client "
run_compare_test tests/chain-client  <<END_OF_TEST_RESULTS
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)

user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt

cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     0

cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1
END_OF_TEST_RESULTS


##### Test tests/command-bad-username #####

runtest() {
make_pwfile
$src/cvm-testclient $src/cvm-pwfile pxfuser '' testpass
}
vecho "Running test tests/command-bad-username "
run_compare_test tests/command-bad-username  <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
END_OF_TEST_RESULTS


##### Test tests/sasl-eof #####

runtest() {
sasltest LOGIN </dev/null
}
vecho "Running test tests/sasl-eof "
run_compare_test tests/sasl-eof  <<END_OF_TEST_RESULTS
sasl-auth-test: AUTH LOGIN PLAIN
+ VXNlcm5hbWU6^M
sasl-auth-test: SASL AUTH LOGIN failed
sasl-auth-test: Fatal: sasl_auth1 failed: 8
  535 End of file reached.
END_OF_TEST_RESULTS


##### Test tests/vmailmgr-xconvert #####

runtest() {
env VMAILMGR_AUTOCONVERT=1 \
$src/cvm-testclient $src/cvm-vmailmgr virt test.tld pass
echo
$src/cvm-testclient $src/cvm-vmailmgr virt test.tld pass
echo
cdbget virt <$home/passwd.cdb | xxd
}
vecho "Running test tests/vmailmgr-xconvert "
run_compare_test tests/vmailmgr-xconvert  <<END_OF_TEST_RESULTS
user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt

user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt

0000000: 020a 0108 0100 2430 2470 6173 7300 2e2f  ......\$0\$pass../
0000010: 7669 7274 0000 002d 002d 002d 002d 0031  virt...-.-.-.-.1
0000020: 3031 3637 3331 3335 3800 2d00            016731358.-.
END_OF_TEST_RESULTS


##### Test tests/pwfile #####

runtest() {
make_pwfile
$src/cvm-testclient $src/cvm-pwfile pwfuser '' testpass
}
vecho "Running test tests/pwfile "
run_compare_test tests/pwfile  <<END_OF_TEST_RESULTS
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
END_OF_TEST_RESULTS


##### Test tests/vmlookup-baddomain #####

runtest() {
CVM_LOOKUP_SECRET=secret \
$src/cvm-testclient $tmp/cvm-vmailmgr-lookup virt other.tld
}
vecho "Running test tests/vmlookup-baddomain "
run_compare_test tests/vmlookup-baddomain  <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1
END_OF_TEST_RESULTS


##### Test tests/sasl-login2 #####

runtest() {
sasltest LOGIN\ cHdmdXNlcg== <<EOF
dGVzdHBhc3M=
EOF
}
vecho "Running test tests/sasl-login2 "
run_compare_test tests/sasl-login2  <<END_OF_TEST_RESULTS
sasl-auth-test: AUTH LOGIN PLAIN
+ UGFzc3dvcmQ6^M
sasl-auth-test: SASL AUTH LOGIN username=pwfuser
END_OF_TEST_RESULTS


##### Test tests/command-case #####

runtest() {
make_pwfile
$src/cvm-testclient $src/cvm-pwfile PWFUser '' testpass
}
vecho "Running test tests/command-case "
run_compare_test tests/command-case  <<END_OF_TEST_RESULTS
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
END_OF_TEST_RESULTS


##### Test tests/vmlookup-pass #####

runtest() {
CVM_LOOKUP_SECRET=secret \
$src/cvm-testclient $tmp/cvm-vmailmgr-lookup virt test.tld
}
vecho "Running test tests/vmlookup-pass "
run_compare_test tests/vmlookup-pass  <<END_OF_TEST_RESULTS
user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt
END_OF_TEST_RESULTS


##### Test tests/vmailmgr-upper-pass #####

runtest() {
$src/cvm-testclient $src/cvm-vmailmgr virt test.tld Pass
}
vecho "Running test tests/vmailmgr-upper-pass "
run_compare_test tests/vmailmgr-upper-pass  <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     0
END_OF_TEST_RESULTS


##### Test tests/vmlookup-upper-domain #####

runtest() {
CVM_LOOKUP_SECRET=secret \
$src/cvm-testclient $tmp/cvm-vmailmgr-lookup virt Test.TLD
}
vecho "Running test tests/vmlookup-upper-domain "
run_compare_test tests/vmlookup-upper-domain  <<END_OF_TEST_RESULTS
user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt
END_OF_TEST_RESULTS


##### Test tests/sasl-login1 #####

runtest() {
sasltest LOGIN <<EOF
cHdmdXNlcg==
dGVzdHBhc3M=
EOF
}
vecho "Running test tests/sasl-login1 "
run_compare_test tests/sasl-login1  <<END_OF_TEST_RESULTS
sasl-auth-test: AUTH LOGIN PLAIN
+ VXNlcm5hbWU6^M
+ UGFzc3dvcmQ6^M
sasl-auth-test: SASL AUTH LOGIN username=pwfuser
END_OF_TEST_RESULTS


##### Test tests/sasl-plain1 #####

runtest() {
sasltest PLAIN <<EOF
cHdmdXNlcgBwd2Z1c2VyAHRlc3RwYXNz
EOF
}
vecho "Running test tests/sasl-plain1 "
run_compare_test tests/sasl-plain1  <<END_OF_TEST_RESULTS
sasl-auth-test: AUTH LOGIN PLAIN
+ ^M
sasl-auth-test: SASL AUTH PLAIN username=pwfuser
END_OF_TEST_RESULTS


##### Test tests/sasl-plain-fail #####

runtest() {
sasltest PLAIN\ cHdmdXNlcgBwd2Z1c2VyAHRlc3RwYXNZ </dev/null
}
vecho "Running test tests/sasl-plain-fail "
run_compare_test tests/sasl-plain-fail  <<END_OF_TEST_RESULTS
sasl-auth-test: AUTH LOGIN PLAIN
sasl-auth-test: SASL AUTH PLAIN failed
sasl-auth-test: Fatal: sasl_auth1 failed: 1
  501 Authentication failed.
END_OF_TEST_RESULTS


##### Test tests/qmail-lookup-baddomain #####

runtest() {
env CVM_LOOKUP_SECRET=secret \
$src/cvm-testclient $tmp/cvm-qmail-lookup user other.dom
}
vecho "Running test tests/qmail-lookup-baddomain "
run_compare_test tests/qmail-lookup-baddomain  <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1
END_OF_TEST_RESULTS


##### Test tests/sasl-plain2 #####

runtest() {
sasltest PLAIN\ cHdmdXNlcgBwd2Z1c2VyAHRlc3RwYXNz </dev/null
}
vecho "Running test tests/sasl-plain2 "
run_compare_test tests/sasl-plain2  <<END_OF_TEST_RESULTS
sasl-auth-test: AUTH LOGIN PLAIN
sasl-auth-test: SASL AUTH PLAIN username=pwfuser
END_OF_TEST_RESULTS


##### Test tests/vmlookup-baduser #####

runtest() {
CVM_LOOKUP_SECRET=secret \
$src/cvm-testclient $tmp/cvm-vmailmgr-lookup missing test.tld
}
vecho "Running test tests/vmlookup-baduser "
run_compare_test tests/vmlookup-baduser  <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     0
END_OF_TEST_RESULTS


##### Test tests/vmlookup-badpass #####

runtest() {
$src/cvm-testclient $src/cvm-vmailmgr virt test.tld 'xpass'
}
vecho "Running test tests/vmlookup-badpass "
run_compare_test tests/vmlookup-badpass  <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     0
END_OF_TEST_RESULTS


##### Test tests/vmailmgr-upper-virt #####

runtest() {
$src/cvm-testclient $src/cvm-vmailmgr Virt test.tld pass
}
vecho "Running test tests/vmailmgr-upper-virt "
run_compare_test tests/vmailmgr-upper-virt  <<END_OF_TEST_RESULTS
user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt
END_OF_TEST_RESULTS


##### Test tests/command-no-prefix #####

runtest() {
make_pwfile
$src/cvm-testclient $src/cvm-pwfile pwfuser '' testpass
}
vecho "Running test tests/command-no-prefix "
run_compare_test tests/command-no-prefix  <<END_OF_TEST_RESULTS
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
END_OF_TEST_RESULTS


##### Test tests/vmailmgr-upper-domain #####

runtest() {
$src/cvm-testclient $src/cvm-vmailmgr virt Test.TLD pass
}
vecho "Running test tests/vmailmgr-upper-domain "
run_compare_test tests/vmailmgr-upper-domain  <<END_OF_TEST_RESULTS
user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt
END_OF_TEST_RESULTS


##### Test tests/pwfile-bad-password #####

runtest() {
make_pwfile
$src/cvm-testclient $src/cvm-pwfile pwfuser '' testpaxx
}
vecho "Running test tests/pwfile-bad-password "
run_compare_test tests/pwfile-bad-password  <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
END_OF_TEST_RESULTS


##### Test tests/vmailmgr-normal #####

runtest() {
$src/cvm-testclient $src/cvm-vmailmgr virt test.tld pass
echo
$src/cvm-testclient $src/cvm-vmailmgr missing test.tld pass
}
vecho "Running test tests/vmailmgr-normal "
run_compare_test tests/vmailmgr-normal  <<END_OF_TEST_RESULTS
user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt

cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     0
END_OF_TEST_RESULTS


##### Test tests/qmail-lookup-nodomain #####

runtest() {
CVM_LOOKUP_SECRET=secret \
$src/cvm-testclient $tmp/cvm-qmail-lookup addr ''
}
vecho "Running test tests/qmail-lookup-nodomain "
run_compare_test tests/qmail-lookup-nodomain  <<END_OF_TEST_RESULTS
user name:        user
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           noathost
mailbox path:     @TMPDIR@/home
END_OF_TEST_RESULTS


##### Test tests/qmail-lookup-perms #####

runtest() {
doit() {
    echo "$1" >$tmp/.qmail-lookaside-default
    $src/cvm-testclient $tmp/cvm-qmail-lookup lookaside-someone test.tld
}

export CVM_LOOKUP_SECRET=secret
export CVM_QMAIL_CHECK_PERMS=0

$src/cvm-testclient $tmp/cvm-qmail-lookup addr test.tld

chmod 000 $home
echo
$src/cvm-testclient $tmp/cvm-qmail-lookup addr test.tld

export CVM_QMAIL_CHECK_PERMS=-
echo
$src/cvm-testclient $tmp/cvm-qmail-lookup addr test.tld

chmod 755 $home

unset CVM_LOOKUP_SECRET
unset CVM_QMAIL_CHECK_PERMS
}
vecho "Running test tests/qmail-lookup-perms "
run_compare_test tests/qmail-lookup-perms  <<END_OF_TEST_RESULTS
user name:        user
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home

cvm-testclient: Authentication failed, error #4 (Input/Output error)

cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     0
END_OF_TEST_RESULTS


##### Test tests/v1lookup #####

runtest() {
local secret="$1"

make_pwfile
cat <<EOF >$tmp/cvmlookup
#!/bin/sh
CVM_LOOKUP_SECRET=$secret
export CVM_LOOKUP_SECRET
exec $src/cvm-pwfile
EOF
chmod +x $tmp/cvmlookup

$src/cvm-v1testclient $tmp/cvmlookup pwfuser ''
$src/cvm-v1testclient $tmp/cvmlookup pwfuser '' ''
$src/cvm-v1testclient $tmp/cvmlookup pwfuser '' secret

rm -f $tmp/cvmlookup
}
vecho "Running test tests/v1lookup ''"
run_compare_test tests/v1lookup '' <<END_OF_TEST_RESULTS
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
END_OF_TEST_RESULTS

vecho "Running test tests/v1lookup 'secret'"
run_compare_test tests/v1lookup 'secret' <<END_OF_TEST_RESULTS
cvm-v1testclient: Fatal: Authentication failed, error #7 (Credential missing in data from module)
cvm-v1testclient: Fatal: Authentication failed, error #7 (Credential missing in data from module)
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
END_OF_TEST_RESULTS


##### Test tests/qmail-lookup-lookaside #####

runtest() {
doit() {
    echo "$1" >$home/.qmail-lookaside-default
    $src/cvm-testclient $tmp/cvm-qmail-lookup lookaside-someone test.tld
}

export CVM_LOOKUP_SECRET=secret
export CVM_QMAIL_LOOKASIDE='/bin/program1 /bin/program2 /bin/program3'

doit '| /bin/program foo'
echo
doit '| /bin/program1 foo'
echo
doit '| /bin/program2 foo'
echo
doit '| /bin/program3 foo'
echo
doit ' |/bin/program1'
echo
doit $'#before\n|/bin/program2\n#after'

unset CVM_LOOKUP_SECRET
unset CVM_QMAIL_LOOKASIDE
}
vecho "Running test tests/qmail-lookup-lookaside "
run_compare_test tests/qmail-lookup-lookaside  <<END_OF_TEST_RESULTS
user name:        user
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home

cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1

cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1

cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1

cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1

cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1
END_OF_TEST_RESULTS


##### Test tests/lookup #####

runtest() {
local secret="$1"

make_pwfile
cat <<EOF >$tmp/cvmlookup
#!/bin/sh
CVM_LOOKUP_SECRET=$secret
export CVM_LOOKUP_SECRET
exec $src/cvm-pwfile
EOF
chmod +x $tmp/cvmlookup

$src/cvm-testclient $tmp/cvmlookup pwfuser ''
env CVM_LOOKUP_SECRET='' \
$src/cvm-testclient $tmp/cvmlookup pwfuser ''
env CVM_LOOKUP_SECRET=$secret \
$src/cvm-testclient $tmp/cvmlookup pwfuser ''

rm -f $tmp/cvmlookup
}
vecho "Running test tests/lookup ''"
run_compare_test tests/lookup '' <<END_OF_TEST_RESULTS
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
END_OF_TEST_RESULTS

vecho "Running test tests/lookup 'secret'"
run_compare_test tests/lookup 'secret' <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #7 (Credential missing in data from module)
cvm-testclient: Authentication failed, error #7 (Credential missing in data from module)
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
END_OF_TEST_RESULTS


##### Test tests/chain-module #####

runtest() {
CVM_CHAIN0=$src/cvm-pwfile
CVM_CHAIN1=$src/cvm-vmailmgr
export CVM_CHAIN0 CVM_CHAIN1

$src/cvm-testclient $src/cvm-chain pwfuser test.tld testpass
echo
$src/cvm-testclient $src/cvm-chain virt test.tld pass
echo
$src/cvm-testclient $src/cvm-chain notexist test.tld pass
echo
$src/cvm-testclient $src/cvm-chain notexist other.dom pass

unset CVM_CHAIN0 CVM_CHAIN1
}
vecho "Running test tests/chain-module "
run_compare_test tests/chain-module  <<END_OF_TEST_RESULTS
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)

user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt

cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     0

cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1
END_OF_TEST_RESULTS


##### Test tests/sasl-nomech #####

runtest() {
sasltest BOGUS </dev/null
}
vecho "Running test tests/sasl-nomech "
run_compare_test tests/sasl-nomech  <<END_OF_TEST_RESULTS
sasl-auth-test: AUTH LOGIN PLAIN
sasl-auth-test: SASL AUTH BOGUS failed
sasl-auth-test: Fatal: sasl_auth1 failed: 2
  504 Unrecognized authentication mechanism.
END_OF_TEST_RESULTS


##### Test tests/command-bad-password #####

runtest() {
make_pwfile
$src/cvm-testclient $src/cvm-pwfile pwfuser '' testpassx
}
vecho "Running test tests/command-bad-password "
run_compare_test tests/command-bad-password  <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
END_OF_TEST_RESULTS


##### Test tests/split #####

runtest() {
local dom1="$1"
local dom2="$2"
$src/cvm-testclient $src/cvm-vmailmgr virt$dom1 "$dom2" pass
}
vecho "Running test tests/split '' ''"
run_compare_test tests/split '' '' <<END_OF_TEST_RESULTS
user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           noathost
mailbox path:     @TMPDIR@/home/virt
END_OF_TEST_RESULTS

vecho "Running test tests/split '' 'test.tld'"
run_compare_test tests/split '' 'test.tld' <<END_OF_TEST_RESULTS
user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt
END_OF_TEST_RESULTS

vecho "Running test tests/split '' 'other.tld'"
run_compare_test tests/split '' 'other.tld' <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1
END_OF_TEST_RESULTS

vecho "Running test tests/split '@test.tld' ''"
run_compare_test tests/split '@test.tld' '' <<END_OF_TEST_RESULTS
user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt
END_OF_TEST_RESULTS

vecho "Running test tests/split '@test.tld' 'test.tld'"
run_compare_test tests/split '@test.tld' 'test.tld' <<END_OF_TEST_RESULTS
user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt
END_OF_TEST_RESULTS

vecho "Running test tests/split '@test.tld' 'other.tld'"
run_compare_test tests/split '@test.tld' 'other.tld' <<END_OF_TEST_RESULTS
user name:        virt
user ID:          @UID@
group ID:         @GID@
real name:        (null)
directory:        @TMPDIR@/home/virt
shell:            (null)
group name:       (null)
system user name: user
system directory: @TMPDIR@/home
domain:           test.tld
mailbox path:     @TMPDIR@/home/virt
END_OF_TEST_RESULTS

vecho "Running test tests/split '@other.tld' ''"
run_compare_test tests/split '@other.tld' '' <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1
END_OF_TEST_RESULTS

vecho "Running test tests/split '@other.tld' 'test.tld'"
run_compare_test tests/split '@other.tld' 'test.tld' <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1
END_OF_TEST_RESULTS

vecho "Running test tests/split '@other.tld' 'other.tld'"
run_compare_test tests/split '@other.tld' 'other.tld' <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
out of scope:     1
END_OF_TEST_RESULTS


##### Test tests/command-prefix #####

runtest() {
make_pwfile
$src/cvm-testclient cvm-command:$src/cvm-pwfile pwfuser '' testpass
}
vecho "Running test tests/command-prefix "
run_compare_test tests/command-prefix  <<END_OF_TEST_RESULTS
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
END_OF_TEST_RESULTS


##### Test tests/start-pwfile-local #####

runtest() {
$src/cvm-pwfile cvm-local:$tmp/socket >$tmp/log &
echo $! >$tmp/pid
sleep 1
cat $tmp/log
}
vecho "Running test tests/start-pwfile-local "
run_compare_test tests/start-pwfile-local  <<END_OF_TEST_RESULTS
Starting.
END_OF_TEST_RESULTS


##### Test tests/v1local #####

runtest() {
make_pwfile
$src/cvm-v1testclient cvm-local:$tmp/socket pwfuser '' testpass
tail -n 1 $tmp/log
}
vecho "Running test tests/v1local "
run_compare_test tests/v1local  <<END_OF_TEST_RESULTS
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
+ pwfuser
END_OF_TEST_RESULTS


##### Test tests/pwfile-local #####

runtest() {
make_pwfile
$src/cvm-testclient cvm-local:$tmp/socket pwfuser '' testpass
tail -n 1 $tmp/log
}
vecho "Running test tests/pwfile-local "
run_compare_test tests/pwfile-local  <<END_OF_TEST_RESULTS
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
+ pwfuser
END_OF_TEST_RESULTS


##### Test tests/pwfile-local-bad-password #####

runtest() {
make_pwfile
$src/cvm-testclient cvm-local:$tmp/socket pwfuser '' testpaxx
tail -n 1 $tmp/log
}
vecho "Running test tests/pwfile-local-bad-password "
run_compare_test tests/pwfile-local-bad-password  <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
- pwfuser
END_OF_TEST_RESULTS


##### Test tests/stop-pwfile-local #####

runtest() {
kill `cat $tmp/pid`
rm -f $tmp/pid
sleep 1
tail -n 1 $tmp/log
echo `cat $tmp/log | wc`
rm -f $tmp/log
test -e $tmp/socket || echo Socket was removed.
}
vecho "Running test tests/stop-pwfile-local "
run_compare_test tests/stop-pwfile-local  <<END_OF_TEST_RESULTS
Stopping.
5 8 50
Socket was removed.
END_OF_TEST_RESULTS


##### Test tests/start-pwfile-udp #####

runtest() {
$src/cvm-pwfile cvm-udp:127.1.2.3:12345 >$tmp/log &
echo $! >$tmp/pid
sleep 1
cat $tmp/log
}
vecho "Running test tests/start-pwfile-udp "
run_compare_test tests/start-pwfile-udp  <<END_OF_TEST_RESULTS
Starting.
END_OF_TEST_RESULTS


##### Test tests/v1udp #####

runtest() {
make_pwfile
$src/cvm-v1testclient cvm-udp:127.1.2.3:12345 pwfuser '' testpass
tail -n 1 $tmp/log
}
vecho "Running test tests/v1udp "
run_compare_test tests/v1udp  <<END_OF_TEST_RESULTS
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
+ pwfuser
END_OF_TEST_RESULTS


##### Test tests/pwfile-udp #####

runtest() {
make_pwfile
$src/cvm-testclient cvm-udp:127.1.2.3:12345 pwfuser '' testpass
tail -n 1 $tmp/log
}
vecho "Running test tests/pwfile-udp "
run_compare_test tests/pwfile-udp  <<END_OF_TEST_RESULTS
user name:        pwfuser
user ID:          123
group ID:         456
real name:        Gecos
directory:        /home/ftp
shell:            /bin/false
group name:       (null)
system user name: (null)
system directory: (null)
domain:           (null)
mailbox path:     (null)
+ pwfuser
END_OF_TEST_RESULTS


##### Test tests/pwfile-udp-bad-password #####

runtest() {
make_pwfile
$src/cvm-testclient cvm-udp:127.1.2.3:12345 pwfuser '' testpaxx
tail -n 1 $tmp/log
}
vecho "Running test tests/pwfile-udp-bad-password "
run_compare_test tests/pwfile-udp-bad-password  <<END_OF_TEST_RESULTS
cvm-testclient: Authentication failed, error #100 (Credentials rejected)
- pwfuser
END_OF_TEST_RESULTS


##### Test tests/stop-pwfile-udp #####

runtest() {
kill `cat $tmp/pid`
rm -f $tmp/pid
sleep 1
tail -n 1 $tmp/log
echo `cat $tmp/log | wc`
rm -f $tmp/log
}
vecho "Running test tests/stop-pwfile-udp "
run_compare_test tests/stop-pwfile-udp  <<END_OF_TEST_RESULTS
Stopping.
5 8 50
END_OF_TEST_RESULTS


rm -rf $tmp
echo $tests_count tests executed, $tests_failed failures
if [ $tests_failed != 0 ]; then exit 1; fi
